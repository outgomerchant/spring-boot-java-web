<html>

<head>
<title>First Web Application</title>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">

<meta name="viewport" content="width=device-width, initial-scale=1">

<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
body {
  margin: 0;
  font-family: "Lato", sans-serif;
}

.sidebar {
  margin: 0;
  padding: 0;
  width: 200px;
  background-color: #f1f1f1;
  position: fixed;
  height: 100%;
  overflow: auto;
}

.sidebar a {
  display: block;
  color: black;
  padding: 16px;
  text-decoration: none;
}
 
.sidebar a.active {
  background-color: #04AA6D;
  color: white;
}

.sidebar a:hover:not(.active) {
  background-color: #555;
  color: white;
}

div.content {
  margin-left: 200px;
  padding: 1px 16px;
  height: 1000px;
}

@media screen and (max-width: 700px) {
  .sidebar {
    width: 100%;
    height: auto;
    position: relative;
  }
  .sidebar a {float: left;}
  div.content {margin-left: 0;}
}

@media screen and (max-width: 400px) {
  .sidebar a {
    text-align: center;
    float: none;
  }
}
</style>
</head>

<body>

<div class="sidebar">
  <a class="active" href="#">Home</a>
  <a href="#index">Orders</a>
  <a href="#">Customers</a>
  <a href="#">Products</a>
</div>

<div class="content">
<h1>Order Information</h1>
<div class="table-responsive">

<table id="example" class="display" >
        <thead>
            <tr>
                <th>Order Number</th>
                <th>Date</th>
                <th>Price</th>
                <th>Discounts</th>
                <th>Total Price</th>
                <th>Email</th>
                <th>Status</th>
            </tr>
        </thead>
        <tfoot>
           <tr>
                <th>Order Number</th>
                <th>Date</th>
                <th>Price</th>
                <th>Discounts</th>
                <th>Total Price</th>
                <th>Email</th>
                <th>Status</th>
            </tr>
        </tfoot>
    </table>
    </div>
    </div>
    
    
    <script>
   $(document).ready(function() {
    $('#example').DataTable( {
        "ajax": "/api",
        "columns": [
            { "data": "name" },
            { "data": "created_at" },
            { "data": "total_price" },
            { "data": "total_discounts" },
            { "data": "total_price_usd" },
            { "data": "email" },
            { "data": "order_status_url" }
        ],
        
         "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return '<a target="_blank" href="'+data+'">Status</a>';
                },
                "targets": 6
            },
            { "visible": false,  "targets": [ 3 ] }
        ]
    } );
} );
    </script>
    
    

</script>
    </body>
    </html>