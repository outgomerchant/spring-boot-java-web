package com.elevati.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.elevati.web.service.OrderService;

/**
 * @author Amol
 *Rest Api Controller
 */
@RestController
@RequestMapping(value = "/api")
public class RestCont {

	@Autowired
	OrderService orderService;

	
	@ResponseBody
	@GetMapping(value = "", produces=MediaType.APPLICATION_JSON_VALUE)
	String index() {
		return orderService.findAllOrders();
	}

	
	@GetMapping(value = "fetch-orders-from-sample-Shopify")
	String fetch() {
		orderService.syncData();
	    return "";
	}
}
