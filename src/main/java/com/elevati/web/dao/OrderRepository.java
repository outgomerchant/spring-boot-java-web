package com.elevati.web.dao;

import org.springframework.data.repository.CrudRepository;

import com.elevati.web.model.Order;

public interface OrderRepository extends CrudRepository<Order, Integer>{
	
}
