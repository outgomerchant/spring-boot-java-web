package com.elevati.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.elevati.web.model.OrdersResponse;

import net.minidev.json.JSONObject;

//import com.elevati.web.dao.OrderDao;
import com.elevati.web.dao.OrderRepository;

@Service
public class OrderService {

		@Autowired
		OrderRepository orderDao;

		

	public void syncData() {
		final String uri = "https://elevatitech.com/api/test/shopify/orders";
		RestTemplate restTemplate = new RestTemplate();
		OrdersResponse result = restTemplate.getForObject(uri, OrdersResponse.class);	
	    System.out.println(result.success);
		  
//	    for (Order order:result.orders)
	    	orderDao.saveAll(result.orders);
	
	}
	
	
	public String findAllOrders() {
		JSONObject obj =new JSONObject();
		obj.appendField("data", orderDao.findAll());
	    return obj.toString()     ;
	}

}