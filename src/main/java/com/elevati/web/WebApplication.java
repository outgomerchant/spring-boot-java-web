package com.elevati.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author Amol 
 *
 */
@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class WebApplication {
	

	public static void main(String[] args) {
	
		SpringApplication.run(WebApplication.class, args);
		
		RestTemplate restTemplate = new RestTemplate();
	    String result = restTemplate.getForObject("http://localhost:9090/api/fetch-orders-from-sample-Shopify", String.class);
	}

}
