package com.elevati.web.model;

import java.util.List;

public class OrdersResponse {

	public boolean success;
    public List<Order> orders;
    public String message;
}
