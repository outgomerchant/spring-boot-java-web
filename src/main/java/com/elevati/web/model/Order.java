package com.elevati.web.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
@Entity
@Table(name = "orders")
public class Order {
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column  
	@Id

    public int order_number;

	@Column  
    public long id;
	@Column  
    public String email;
	@Column  
    @JsonFormat(pattern="yyyy-MM-dd")
    public Date created_at;
	@Column  
    public Date updated_at;
	@Column  
    public int number;
	@Column  
    public double total_price;
	@Column  
    public boolean confirmed;
	@Column  
    public int total_discounts;
	@Column  
    public String name;
	@Column  
    public double total_price_usd;
	@Column  
    public long user_id;
	@Column  
    public Date processed_at;
	@Column  
    public int app_id;
	@Column  
    public String contact_email;
	@Column  
    public String order_status_url;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCreated_at() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
		String strDate = dateFormat.format(created_at);  

		return strDate;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getTotal_price() {
		return total_price;
	}
	public void setTotal_price(double total_price) {
		this.total_price = total_price;
	}
	public boolean isConfirmed() {
		return confirmed;
	}
	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}
	public int getTotal_discounts() {
		return total_discounts;
	}
	public void setTotal_discounts(int total_discounts) {
		this.total_discounts = total_discounts;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getTotal_price_usd() {
		return total_price_usd;
	}
	public void setTotal_price_usd(double total_price_usd) {
		this.total_price_usd = total_price_usd;
	}
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public Date getProcessed_at() {
		return processed_at;
	}
	public void setProcessed_at(Date processed_at) {
		this.processed_at = processed_at;
	}
	public int getApp_id() {
		return app_id;
	}
	public void setApp_id(int app_id) {
		this.app_id = app_id;
	}
	public int getOrder_number() {
		return order_number;
	}
	public void setOrder_number(int order_number) {
		this.order_number = order_number;
	}
	public String getContact_email() {
		return contact_email;
	}
	public void setContact_email(String contact_email) {
		this.contact_email = contact_email;
	}
	public String getOrder_status_url() {
		return order_status_url;
	}
	public void setOrder_status_url(String order_status_url) {
		this.order_status_url = order_status_url;
	}
	
}


